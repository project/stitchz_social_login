<?php
/**
 * @file
 * Stitchz login module - social  login.
 */

 $restricted_resource_url = 'https://api.stitchz.net/';

/**
 * Implements hook_help().
 */
function stitchz_social_login_help($path, $arg) {
  switch ($path) {
    case "admin/help#stitchz_social_login":
      return t('<h3>Contact Us</h3><p>Please reach out to Stitchz for any questions or feedback. We can be found on <a href="@facebook_url">Facebook</a> and <a href="@twitter_url">twitter</a> or on our website at <a href="@stitchz_contact_us">@stitchz_contact_us</a>. Our support forum can be found at <a href="@support_forum">@support_forum</a> or we can be contacted via email at <a href="mailto:@support_email">@support_email</a>.</p>', array(
        '@facebook_url' => 'https://www.facebook.com/stitchz.net',
        '@twitter_url' => 'https://twitter.com/stitchzdotnet',
        '@stitchz_contact_us' => 'http://www.stitchz.net/ContactUs',
        '@support_forum' => 'http://stitchz.uservoice.com/forums/81839?lang=en',
        '@support_email' => 'support@stitchz.net',
      ));
  }
}

/**
 * Implements hook_menu().
 */
function stitchz_social_login_menu() {
  $items['admin/config/people/stitchz_social_login'] = array(
    'title' => 'Stitchz Social Login',
    'description' => 'Configure how your users login with their Social Login identity',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stitchz_social_login_config_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'stitchz_social_login.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_user_delete().
 *
 * Cleans up identity table when the user is deleted.
 */
function stitchz_social_login_user_delete($account) {

  // Get authmap id for given user.
  $sql = "SELECT a.aid FROM {authmap} a WHERE a.module = 'stitchz_social_login' AND a.uid = :uid";
  $authmapid = db_query($sql, array(':uid' => $account->uid))
    ->fetchField();

  // Check the authmap id is valid, then remove the identity.
  if (is_numeric($authmapid)) {

    db_delete('stitchz_social_login_identities')
      ->condition('authmapid', $authmapid)
      ->execute();
  }
}

/**
 * Clears all settings from database.
 *
 * @param object $form
 *   Drupal form object.
 * @param object $form_state
 *   Byref Drupal form state.
 *
 * @return object
 *   The form with empty field values.
 */
function stitchz_social_login_config_settings_form_remove_settings($form, &$form_state) {

  // Remove drupal form values.
  form_state_values_clean($form_state);

  db_delete('stitchz_social_login_config_settings')
    ->execute();

  drupal_set_message(t('Stitchz Login Configuration Settings Removed'));

  cache_clear_all();

  $form['stitchz_social_login_config_settings_form']['apikey']['#value'] = '';
  $form['stitchz_social_login_config_settings_form']['appsecret']['#value'] = '';
  $form['stitchz_social_login_config_settings_form']['redirecturl']['#value'] = '';
  $form['stitchz_social_login_config_settings_form']['version']['#value'] = '';
  $form['stitchz_social_login_config_settings_form']['domain']['#value'] = '';

  $form_state['values'] = array();
  $form_state['rebuild'] = TRUE;

  return $form['stitchz_social_login_config_settings_form'];
}

/**
 * Send request to Stitchz for OAuth2 access token.
 *
 * Access tokens have a short life so they are stored in the user's session for
 * quick access and to save unnecessary HTTP calls.
 *
 * @param string $dnsalias
 *   The app url as provided by Stitchz.
 * @param string $apikey
 *   The unique client id provided by Stitchz.
 * @param string $appsecret
 *   The application secret provided by Stitchz.
 *
 * @return string|bool
 *   A valid oauth 2 access token or FALSE if the request failed.
 */
function stitchz_social_login_get_access_token($dnsalias, $apikey, $appsecret) {

  $dnsalias = rtrim($dnsalias, '/') . '/';
  $url = url($dnsalias . 'api/oauth2/Token', array('external' => TRUE));

  // Check if a previous session exists with access_token, saves an HTTP call.
  if (isset($_SESSION['stitchz_social_login_access_token'])  && !empty($_SESSION['stitchz_social_login_access_token'])) {
    if (strtotime($_SESSION['stitchz_social_login_access_token']['expiry_time']) >= REQUEST_TIME) {
      return $_SESSION['stitchz_social_login_access_token']['access_token'];
    }
  }

  $data = drupal_http_build_query(array(
    'client_id' => $apikey,
    'client_secret' => $appsecret,
    'grant_type' => 'client_credentials',
    'format' => 'json',
  ));

  $options = array(
    'method' => 'POST',
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'Accept' => 'application/json',
    ),
    'data' => $data,
  );

  // TODO: we could use CURL here or give a choice of default Drupal vs. Curl.
  $response = drupal_http_request($url, $options);

  // Check for a valid response.
  if (is_object($response) && $response->code) {
    switch ($response->code) {
      case '200':
        if (!empty($response->data)) {
          $json_response = drupal_json_decode($response->data);

          // Get the Access Token out of the response.
          $access_token = $json_response['access_token'];
          $expiry_time = $json_response['expires_in'];

          // Check if the token is there.
          if (!empty($access_token) && !empty($expiry_time)) {

            // Save access token and expiry_time in session object.
            $_SESSION['stitchz_social_login_access_token'] = array();
            $_SESSION['stitchz_social_login_access_token']['access_token'] = $access_token;
            $_SESSION['stitchz_social_login_access_token']['expiry_time'] = format_date(REQUEST_TIME + $expiry_time, 'custom', 'm/d/Y h:i:s a');

            return $access_token;
          }
        }
        break;

      case '401':
        watchdog('Authentication Request', '@url response code was @code - @status_message', array(
          '@url' => $url,
          '@code' => $response->code,
          '@status_message' => $response->status_message,
          ), WATCHDOG_WARNING);
        return '<div class="messages warning"><ul><li>' . t('Unauthorized request. Double check your API Key and App Secret and try again.') . '</li></ul></div>';

      case '404':
        watchdog('Authentication Request', '@url response code was @code - @status_message', array(
          '@url' => $url,
          '@code' => $response->code,
          '@status_message' => $response->status_message,
          ), WATCHDOG_WARNING);
        return '<div class="messages warning"><ul><li>' . t('Not Found. Double check your relaying party URL and try again.') . '</li></ul></div>';

      case '500':
        watchdog('Authentication Request', '@url response code was @code - @status_message', array(
          '@url' => $url,
          '@code' => $response->code,
          '@status_message' => $response->status_message,
          ), WATCHDOG_WARNING);
        return '<div class="messages error"><ul><li>' . t('Internal Server Error. Ensure the service provider is available and contact your service provider.') . '</li></ul></div>';
    }
  }
  else {
    watchdog('Authentication Request', 'Unknown error at @url',
      array(
        '@url' => $url,
      ), WATCHDOG_WARNING);
    return '<div class="messages error"><ul><li>' . t('Error sending/receiving request to service provider. Check all settings and try again.') . '</li></ul></div>';
  }

  return FALSE;
}

/**
 * Gets list of identity providers and returns html.
 *
 * @param object $form
 *   Drupal form object.
 * @param object $form_state
 *   Byref Drupal form state.
 *
 * @return string
 *   An html string of identity providers.
 */
function stitchz_social_login_config_settings_provider_list_api_call($form, &$form_state) {

  $dnsalias = !empty($form_state['values']['domain']) ? $form_state['values']['domain'] : 'https://api.stitchz.net/';
  $apikey = !empty($form_state['values']['apikey']) ? $form_state['values']['apikey'] : "0000000000";
  $appsecret = !empty($form_state['values']['appsecret']) ? $form_state['values']['appsecret'] : "0000000000";
  $version = $form_state['values']['version'];
  $redirecturl = $form_state['values']['redirecturl'];

  $dnsalias = rtrim($dnsalias, '/') . '/';
  $url = url($dnsalias . 'api/v2/providers', array('external' => TRUE));

  $access_token = stitchz_social_login_get_access_token($dnsalias, $apikey, $appsecret);

  if (!empty($access_token)) {
    // Check for error response.
    if (strpos($access_token, '<div') !== FALSE) {
      return $access_token;
    }

    // Send a generic API request.
    $json_providers = stitchz_social_login_get_api_call($url, $access_token);

    if (!is_array($json_providers)) {
      // Check for error response.
      if (strpos($json_providers, '<div') !== FALSE) {
        return $json_providers;
      }
    }
    else {
      $scope = $json_providers['Scope'];
      $notes = '';
      $providers = (isset($json_providers['Providers']) ? $json_providers['Providers'] : array());
      $provider_html = stitchz_social_login_admin_format_provider_list($dnsalias, $providers, $version, $apikey, $redirecturl, $scope, $notes);

      $commands = array();

      $commands[] = ajax_command_html('#stitchz_social_login_provider_list_container', drupal_render($provider_html));

      // Remove all double quotes which cause problems later.
      $um = '<input type="hidden" name="syncd_providers" value="' . str_replace('"', '\'', json_encode($json_providers)) . '" />';
      $commands[] = ajax_command_html('#hidden_providers', render($um));

      $um = '<input type="hidden" name="scope" value="' . $scope . '" />';
      $commands[] = ajax_command_html('#hidden_scope', render($um));

      return array('#type' => 'ajax', '#commands' => $commands);
    }
  }

  return FALSE;
}

/**
 * Ajax function which removes all providers from the database.
 *
 * @param object $form
 *   Drupal form object.
 * @param object $form_state
 *   Byref Drupal form state.
 *
 * @return string
 *   The message of result.
 */
function stitchz_social_login_config_settings_clear_providers($form, &$form_state) {

  $sql = "SELECT s.slcsid FROM {stitchz_social_login_config_settings} s";
  $slc = db_query($sql)->fetchField();

  if (is_numeric($slc)) {
    db_update('stitchz_social_login_config_settings')
      ->fields(array('syncd_providers' => ''))
      ->condition('slcsid', $slc, '=')
      ->execute();

    $commands = array();

    $commands[] = ajax_command_remove('#edit-clear-providers');

    $default_value = t('Your providers list is currently empty. Click "Sync Providers" to pull your list of configured identity providers from your Stitchz Login application.');
    $commands[] = ajax_command_html('#stitchz_social_login_provider_list_container', $default_value);

    return array('#type' => 'ajax', '#commands' => $commands);
  }

  return FALSE;
}

/**
 * Sends authorized HTTP request to Stitchz api.
 *
 * @param string $url
 *   The url to send api request.
 * @param string $access_token
 *   A valid oauth access token.
 *
 * @return json
 *   A valid json object.
 */
function stitchz_social_login_get_api_call($url, $access_token) {

  if (!empty($access_token)) {
    $options = array(
      'method' => 'GET',
      'headers' => array(
        'Authorization' => 'Bearer ' . $access_token,
        'Accept' => 'application/json',
      ),
    );

    // TODO: we could use CURL here or give a choice of default Drupal vs. Curl.
    $response = drupal_http_request($url, $options);

    if (is_object($response) && $response->code) {
      switch ($response->code) {
        case '200':
          if (!empty($response->data)) {
            $json_response = drupal_json_decode($response->data);
            return $json_response;
          }
          break;

        case '401':
          watchdog('API Request', '@url response code was @code - @status_message', array(
            '@url' => $url,
            '@code' => $response->code,
            '@status_message' => $response->status_message,
            ), WATCHDOG_WARNING);
          return '<div class="messages warning"><ul><li>' . t('Unauthorized request. Double check your API Key and App Secret and try again.') . '</li></ul></div>';

        case '404':
          watchdog('API Request', '@url response code was @code - @status_message', array(
            '@url' => $url,
            '@code' => $response->code,
            '@status_message' => $response->status_message,
            ), WATCHDOG_WARNING);
          return '<div class="messages warning"><ul><li>' . t('Not Found. Double check your relaying party URL and try again.') . '</li></ul></div>';

        case '500':
          watchdog('API Request', '@url response code was @code - @status_message', array(
            '@url' => $url,
            '@code' => $response->code,
            '@status_message' => $response->status_message,
            ), WATCHDOG_WARNING);
          return '<div class="messages error"><ul><li>' . t('Internal Server Error. Ensure the service provider is available and contact your service provider.') . '</li></ul></div>';
      }
    }
    else {
      watchdog('API Request', 'Unknown error at @url',
        array(
          '@url' => $url,
        ), WATCHDOG_WARNING);
      return '<div class="messages error"><ul><li>' . t('Error sending/receiving request to service provider. Check all settings and try again.') . '</li></ul></div>';
    }
  }
  return FALSE;
}

/**
 * Implements hook_form_submit().
 *
 * Save all configuration settings to the database.
 */
function stitchz_social_login_config_settings_form_submit($form, &$form_state) {

  // Remove drupal form values.
  form_state_values_clean($form_state);

  $values = array(
    'apikey' => $form_state['values']['apikey'],
    'redirecturl' => $form_state['values']['redirecturl'],
    'domain' => $form_state['values']['domain'],
    'version' => $form_state['values']['version'],
    'appsecret' => $form_state['values']['appsecret'],
    'syncd_providers' => (!empty($form_state['values']['syncd_providers']) ? $form_state['values']['syncd_providers'] : ''),
    'scope' => (!empty($form_state['values']['scope']) ? $form_state['values']['scope'] : 'Basic'),
    'theme_version' => (!empty($form_state['values']['theme_version']) ? $form_state['values']['theme_version'] : 'Basic'),
    'enable_user_login_screen' => (!empty($form_state['values']['enable_user_login_screen']) ? $form_state['values']['enable_user_login_screen'] : '0'),
    'enable_user_login_block_screen' => (!empty($form_state['values']['enable_user_login_block_screen']) ? $form_state['values']['enable_user_login_block_screen'] : '0'),
    'enable_user_register_form_screen' => (!empty($form_state['values']['enable_user_register_form_screen']) ? $form_state['values']['enable_user_register_form_screen'] : '0'),
    'enable_comment_form_screen' => (!empty($form_state['values']['enable_comment_form_screen']) ? $form_state['values']['enable_comment_form_screen'] : '0'),
    'notes' => (!empty($form_state['values']['notes']) ? $form_state['values']['notes'] : ''),
  );

  $sql = "SELECT s.slcsid FROM {stitchz_social_login_config_settings} s";
  $slc = db_query($sql)->fetchField();

  db_merge('stitchz_social_login_config_settings')
    ->key(array('slcsid' => $slc))
    ->insertFields(array('slcsid' => NULL))
    ->fields(array(
      'apikey' => $values['apikey'],
      'domain' => $values['domain'],
      'redirecturl' => $values['redirecturl'],
      'version' => $values['version'],
      'appsecret' => $values['appsecret'],
      'syncd_providers' => $values['syncd_providers'],
      'scope' => $values['scope'],
      'theme_version' => $values['theme_version'],
      'enable_user_login_screen' => $values['enable_user_login_screen'],
      'enable_user_login_block_screen' => $values['enable_user_login_block_screen'],
      'enable_user_register_form_screen' => $values['enable_user_register_form_screen'],
      'enable_comment_form_screen' => $values['enable_comment_form_screen'],
      'notes' => $values['notes'],
    ))
    ->execute();

  drupal_set_message(t('Stitchz Login Configuration Settings Saved.'));
}

/**
 * Get all the configuration settings from the database.
 *
 * @return SelectQueryInterface
 *   The result set of configuration settings.
 */
function stitchz_social_login_config_settings_get_settings() {

  $sql = "SELECT s.* FROM {stitchz_social_login_config_settings} s";
  $results = db_query($sql)->fetchAssoc();

  return $results;
}
